# **Gitlab CE docker-compose**

# **Docker setup and deployment**

### Assuming a fresh Ubuntu 20.04 install (also tested on 22.04)

#### **Install Docker and docker-compose**
1. [Install Docker](https://docs.docker.com/get-docker/)
2. [Install Docker Compose](https://docs.docker.com/compose/).
```bash
sudo apt update

sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce

sudo systemctl status docker

sudo curl -L https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Create docker group if it doesn't exist
sudo groupadd docker
# Add your user to the docker group
sudo usermod -aG docker $USER
# Run the following command or Logout and login again and run (that doesn't work you may need to reboot your machine first)
newgrp docker

# Note: If the command docker-compose fails after installation, check your path. You can also create a symbolic link to /usr/bin or any other directory in your path.
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```
## **Configuration for Docker Compose**
This configuration defines what containers we want to run. In our case, it will be the GitLab service with one GitLab runner (a separate module for running CI / CD tasks) and GitLab container registry(a separate module for store container images for each GitLab project). The most important configuration parameters are:

* **image** – docker image that we want to use in our server
* **ports** – a list of ports that we make available outside the container. In our configuration, we provide ports 80, 443 (website)
* **container_name** – the name of our container
* **volumes** – specifies the volumes that are used by the container. In our configuration, we have directories shared with our system (subdirectories in $ GITLAB_HOME) and an additional volume that allows access to the Docker environment from the GitLab runner.
* **networks** – defines the virtual network in which the containers will operate. In our case, the www portal and the runner operate in one “gitlab-network”

## **Set up the volumes location**
Before setting everything else, configure a new environment variable $GITLAB_HOME pointing to the directory where the configuration, logs, and data files will reside. Ensure that the directory exists and appropriate permission have been granted.

### For **Linux** users, set the path to /srv/gitlab:
```bash
export GITLAB_HOME=/srv/gitlab
```

### For **macOS** users, use the user’s $HOME/gitlab directory:
```bash
export GITLAB_HOME=$HOME/gitlab
```
### **The GitLab container uses host mounted volumes to store persistent data:**
| Local location    |Container location| Usage                        |
|-------------------|------------------|------------------------------|
|$GITLAB_HOME/data  |/var/opt/gitlab   |For storing application data. |
|$GITLAB_HOME/logs  |/var/log/gitlab   |For storing logs.             |
|$GITLAB_HOME/config|/etc/gitlab	   |For storing the GitLab configuration files.|

### **Fetch, compile and deploy**
```bash
git clone https://gitlab.com/lightcyphers-open/csirt/gitlab-compose ~/gitlab

cd ~/gitlab

cp .env.example .env

# To start the app up
docker-compose up
# To run in background
docker-compose up -d
# To run in background
docker-compose logs -f
# To configure nginx see /nginx/README.md
```

### **After starting the docker image**
To log in to GitLab for the first time, you need a temporary password, which is generated automatically during installation. We get the password using the command:
```bash
docker exec -it gitlab-ce grep 'Password:' /etc/gitlab/initial_root_password
```
### **GitLab launching**
Our GitLab is available at: http://localhost:8080. After going to this address, the following screen should appear:

![Example Image](imgages/1.png)

<span style="color: red;">**Note**</span>
**:** The first launch of the portal may take several minutes.

To log in to the portal, we must enter the “root” in Username field and the temporary password that we obtained earlier in the Password field.
After logging in, the following screen should appear:

![Example Image](imgages/2.png)

Congratulations, GitLab has been successfully launched!
### **Initial configuration of the portal**
Before we proceed, it is worth changing a few portal settings. First, we’ll turn off open registration for everyone. To do this, click the View Setting button available on the upper bar with a warning (address to the panel; http://localhost:8080/admin/application_settings/general#js-signup-settings ). On the new page, uncheck Sign-up enabled and save the changes.

![Example Image](imgages/3.png)

The next step should be to change the root user. To do this, go to the website: http://localhost:8080/-/profile/account and enter the name in the Change username field. We approve by clicking on Update username.

![Example Image](imgages/4.png)

The last change we will make is to change the password. To do this, go to the page: http://localhost:8080/-/profile/password/edit and enter a temporary password and a new password. We approve the change by clicking Save password.

## **GitLab runner configuration**
To use the GitLab runner in GitLab, you need to configure it. For correct configuration, we will need a token copied from the portal. To do this, go to the address: http://localhost:8080/admin/runners and click the **Copy registration token** button.

![Example Image](imgages/5.png)

In the next step, it goes to the console and run the following command:
```bash
docker exec -it gitlab-runner gitlab-runner register --url "http://gitlab-ce" --clone-url "http://gitlab-ce"
```
After launching, a configuration module will appear. The module provides the following information:

* Enter the GitLab instance URL: confirm the entered value (click enter)
* Enter the registration token: enter the token copied before.
* Enter a description for the runner: enter the name of the runner, e.g. docker-runner
* Enter tags for the runner: leave the field blank here
* Enter an executor: enter docker here
* Enter the default Docker image: here we provide the default docker image, e.g. docker:20.10.16

After proper configuration, we should see confirmation Runner registred successfully:

![Example Image](imgages/6.png)

In addition to the basic configuration, we also need to allow access for containers launched from the runner to the virtual network in which GitLab operates. To do this, we run the editor (e.g. nano)
```bash
sudo nano gitlab/gitlab-runner/config.toml
```

Then we add new line to the end of the runner configuration: network_mode = "gitlab-network"

![Example Image](imgages/7.png)

Check if in the runner configuration:
volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]

To check if the runner is available from the GitLab level, go to the following address:http://localhost:8080/admin/runners

![Example Image](imgages/8.png)

# **Gitlab-runner docker-compose**

## Usage

- Copy the .env.example to .env
- Edit .env to match runner name and registration_token
- Run `docker-compose -f docker-compose.runner.yml --env-file .env up -d`