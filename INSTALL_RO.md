# **Gitlab CE docker-compose**

# **Configurarea și implementarea Docker**

### Presupunând o instalare proaspătă a Ubuntu 20.04 (testat și pe 22.04)

#### **Instalare Docker si docker-compose**
1. [Instalare Docker](https://docs.docker.com/get-docker/)
2. [Instalare Docker Compose](https://docs.docker.com/compose/).
```bash
sudo apt update

sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce

sudo systemctl status docker

sudo curl -L https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Creați grupul docker dacă nu există
sudo groupadd docker
# Adăugați utilizatorul în grupul docker
sudo usermod -aG docker $USER
# Rulați următoarea comandă sau Deconectați-vă și conectați-vă din nou și rulați (daca aceasta nu funcționează, poate fi necesar să reporniți mai întâi mașina)
newgrp docker

# Notă: Dacă comanda docker-compose eșuează după instalare, verificați calea. De asemenea, puteți crea o legătură simbolică către /usr/bin sau orice alt director din calea dvs.
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```
## **Configurare pentru Docker Compose**
Această configurație definește ce containere dorim să rulăm. În cazul nostru, va fi serviciul GitLab cu un runner GitLab (un modul separat pentru rularea sarcinilor CI / CD) și registrul de containere GitLab (un modul separat pentru stocare imaginilor pentru fiecare proiect GitLab). Cei mai importanți parametri de configurare sunt:

* **image** – imaginea docker pe care dorim să o folosim pe serverul nostru
* **ports** – o listă de porturi pe care le punem la dispoziție în afara containerului. În configurația noastră, oferim porturile 80, 443 (site-ul web)
* **container_name** – numele containerului nostru
* **volumes** – specifică volumele care sunt utilizate de container. În configurația noastră, avem directoare partajate cu sistemul nostru (subdirectoare în $ GITLAB_HOME) și un volum suplimentar care permite accesul la mediul Docker din runnerul GitLab.
* **networks** – definește rețeaua virtuală în care vor funcționa containerele. În cazul nostru, portalul www și runnerul funcționează in aceiasi retea “gitlab-network”

## **Configurați locația volumelor**
Înainte de a seta orice altceva, configurați o nouă variabilă de mediu $GITLAB_HOME care să indice directorul în care vor fi stocate fișierele de configurare, jurnalele și datele. Asigurați-vă că directorul există și că au fost acordate permisiunile corespunzătoare.

### Pentru utilizatorii **Linux**, setați calea la /srv/gitlab:
```bash
export GITLAB_HOME=/srv/gitlab
```

### Pentru utilizatorii **macOS**, utilizați directorul $HOME/gitlab al utilizatorului:
```bash
export GITLAB_HOME=$HOME/gitlab
```
### **Containerul GitLab utilizează volume montate pe gazdă pentru a stoca date persistente:**
| Locația locală    |Locația containerului| Utilizare                                       |
|-------------------|---------------------|-------------------------------------------------|
|$GITLAB_HOME/data  |/var/opt/gitlab      |Pentru stocarea datelor aplicației.              |
|$GITLAB_HOME/logs  |/var/log/gitlab      |Pentru stocarea jurnalelor.                      |
|$GITLAB_HOME/config|/etc/gitlab	      |Pentru stocarea fișierelor de configurare GitLab.|

### **Clonati, compilați și implementați**
```bash
git clone https://gitlab.com/lightcyphers-open/csirt/gitlab-compose ~/gitlab

cd ~/gitlab

cp .env.example .env

# Pentru a porni aplicația
docker-compose up
# Pentru a rula în fundal
docker-compose up -d
# Pentru a rula în fundal
docker-compose logs -f
# Pentru a configura nginx, consultați /nginx/README.md
```

### **După pornirea imaginii docker**
Pentru a vă conecta la GitLab pentru prima dată, aveți nevoie de o parolă temporară, care este generată automat în timpul instalării. Obținem parola folosind comanda:
```bash
docker exec -it gitlab-ce grep 'Password:' /etc/gitlab/initial_root_password
```
### **GitLab launching**
GitLab-ul nostru este disponibil la: http://localhost:8080. După ce mergeți la această adresă, ar trebui să apară următorul ecran:

![Example Image](imgages/1.png)

<span style="color: red;">**Notă**</span>
**:** Prima lansare a portalului poate dura câteva minute.

Pentru a vă conecta la portal, trebuie să introducem “root” în câmpul Nume de utilizator și parola temporară pe care am obținut-o mai devreme în câmpul Parolă.
După autentificare, ar trebui să apară următorul ecran:

![Example Image](imgages/2.png)

Felicitări, GitLab a fost lansat cu succes!
### **Configurarea inițială a portalului**
Înainte de a continua, merită să schimbați câteva setări ale portalului. În primul rând, vom dezactiva înregistrarea deschisă pentru toată lumea. Pentru a face acest lucru, faceți clic pe butonul Vizualizare setări disponibil în bara de sus cu un avertisment (adresa paginii; http://localhost:8080/admin/application_settings/general#js-signup-settings ). Pe noua pagină, debifați Înregistrare activată și salvați modificările.

![Example Image](imgages/3.png)

Următorul pas ar trebui să fie schimbarea utilizatorului root. Pentru a face acest lucru, accesați site-ul web: http://localhost:8080/-/profile/account și introduceți numele în câmpul Schimbă numele. Aprobam făcând clic pe Actualizare nume de utilizator.

![Example Image](imgages/4.png)

Ultima modificare pe care o vom face este schimbarea parolei. Pentru a face acest lucru, accesați pagina: http://localhost:8080/-/profile/password/edit și introduceți o parolă temporară și o nouă parolă. Aprobam modificarea făcând clic pe Salvare parolă.

## **Configurare GitLab runner**
Pentru a utiliza runnerul GitLab în GitLab, trebuie să îl configurați. Pentru configurarea corectă, vom avea nevoie de un token copiat de pe portal. Pentru a face acest lucru, accesați adresa: http://localhost:8080/admin/runners și faceți clic pe butonul **Copiați simbolul de înregistrare**.

![Example Image](imgages/5.png)

În pasul următor, deschideti consola si rulati următoarea comandă:
```bash
docker exec -it gitlab-runner gitlab-runner register --url "http://gitlab-ce" --clone-url "http://gitlab-ce"
```
După lansare, va apărea un modul de configurare. Modulul oferă următoarele informații:

* Introduceți adresa URL a instanței GitLab: confirmați valoarea introdusă (faceți clic pe Enter)
* Introduceți jetonul de înregistrare: introduceți jetonul copiat anterior.
* Introduceți o descriere pentru alergător: introduceți numele alergătorului, de ex. docker-runner
* Introduceți etichete pentru alergător: lăsați câmpul necompletat aici
* Introduceți un executor: introduceți aici docker
* Introduceți imaginea Docker implicită: aici oferim imaginea Docker implicită, de ex. docker:20.10.16

După configurarea corectă, ar trebui să vedem confirmarea că Runner a înregistrat cu succes:

![Example Image](imgages/6.png)

Pe lângă configurația de bază, trebuie să permitem și accesul containerelor lansate din runner către rețeaua virtuală în care operează GitLab. Pentru a face acest lucru, rulăm editorul (de exemplu, nano)
```bash
sudo nano gitlab/gitlab-runner/config.toml
```

Apoi adăugăm o linie nouă la sfârșitul configurației runnerului: network_mode = "gitlab-network"

![Example Image](imgages/7.png)

Verificati daca in configurației runnerului: 
volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]

Pentru a verifica dacă runnerul este disponibil de la nivelul GitLab, accesați următoarea adresă: http://localhost:8080/admin/runners

![Example Image](imgages/8.png)


# **Gitlab-runner docker-compose**

## Utilizare

- Copiați .env.example în .env
- Editați .env pentru a se potrivi cu numele runner-ului și registration_token
- Rulati `docker-compose -f docker-compose.runner.yml --env-file .env up -d`